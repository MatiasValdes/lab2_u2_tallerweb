// Creacion de funcion
function buscar_vocales(){
    // Pedimos la frase
    let frase = document.getElementById("frase").value;
    // Creamos un contador para ir registrando la cantidad de vocales
    let contador_A = 0;
    let contador_E = 0;
    let contador_I = 0;
    let contador_O = 0;
    let contador_U = 0;
    // Validamos de que no venga vacio
    if(frase ==""){
        alert("Favor de ingresar algo");
    }else{
        // Recorreremos la frase y buscaremos las vocales
        for(let i = 0; i < frase.length;i++){
            // ocuparemos tolowercase para transformar todos los caracteres a minusculas y comparar
            // Iremos buscando las vocales, primeramente la A
            if(frase[i].toLowerCase() == "a"){
                // Aumento de contador
                contador_A++;
            } // Si es que es una E se aumenta su contador  
            else if(frase[i].toLowerCase() == "e"){
                contador_E++;
            } // SI es que es una I se aumenta su contador
            else if(frase[i].toLowerCase() == "i"){
                contador_I++;
            }// Si es que es una O se aumenta
            else if(frase[i].toLowerCase() == "o"){
                contador_O++;
            } // Si es que es una U se aumenta
            else if(frase[i].toLowerCase() == "u"){
                contador_U++;
            }
        }
        // Si todos los contadores son 0 es porque no se encontraron vocales
        if(contador_A==0 && contador_E ==0 && contador_I ==0 && contador_O ==0 && contador_U ==0){
            alert("No se encontraron vocales")
        }else{ // Si al menos uno ya está lleno es porque encontró algo
        // Luego que termine de recorrer se da aviso de la cantidad de cuantas veces se repite
        alert("La cantidad de vocal A en la frase " + frase + " es de " + contador_A +
        "\nLa cantidad de vocal E en la frase " + frase + " es de " + contador_E +
        "\nLa cantidad de vocal I en la frase " + frase + " es de " + contador_I + 
        "\nLa cantidad de vocal O en la frase " + frase + " es de " + contador_O +
        "\nLa cantidad de vocal U en la frase " + frase + " es de " + contador_U);
        }
    }
}
