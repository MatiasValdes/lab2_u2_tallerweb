// Permite llevar el conteo de imagen
var numero=1;
// Creacion de función adelante
function adelante(){
    // Cuando se pulse adelante aumentaremos el valor de numero
    numero++;
    // En caso de que sea mayor a 5, que es el limite debe volver al inicio
    if(numero>5){
        numero=1;
    }
    // Obtenemos la imagen y le cambiamos la ruta segun el numero que posea
    var foto = document.getElementById("foto")
    foto.src="../fotos/escudo"+numero+".png";
}

// Creacion de funcion atras
function atras(){
    // Cuando se pulse atrás, disminuiremos el valor del numero
    numero--;
    // Si el numero llega a 0, se debe cambiar a 5 que es el ultimo
    if(numero<1){
        numero=5;
    }
    // Obtenemos la imagen y le cambiamos la ruta
    var foto = document.getElementById("foto")
    foto.src="../fotos/escudo"+numero+".png";
}
