// Creamos la funcion de convertir dolares
function transformar_dolar(){
    // Capturamos la cantidad que se ingreso
    var cambio = document.getElementById("dolar").value;
    // Lo transformamos a float en caso de que ingrese decimal
    cambio = parseFloat(cambio)
    // Validaremos de que no esté vacio
    if(cambio == ""){
        alert("No ha ingresado un numero")
    } // Que no sea nulo
    else if(isNaN(cambio) == true){
        alert("Ha ingresado un valor no numerico")
    }
    // Finalmente generaremos la transformación y lo colocaremos
    // Dolar hoy = 852
    else{
        // Transformamos a clp multiplicando 
        cambio = cambio * 852;
        // Lo colocamos en el input especificado para la transformación
        document.getElementById("transformado").value = cambio;
    }
    
}


// Funcion para convertir clp

function transformar_clp(){
    // Capturamos la cantidad que se ingreso
    var cambio_2 = document.getElementById("pesos_chileno").value;
    // Lo transformamos a float en caso de que ingrese decimal
    cambio_2 = parseFloat(cambio_2)
    // Validaremos de que no esté vacio
    if(cambio_2 == ""){
        alert("No ha ingresado un numero")
    } // Que no sea nulo
    else if(isNaN(cambio_2) == true){
        alert("Ha ingresado un valor no numerico")
    }
    // Finalmente generaremos la transformación y lo colocaremos
    // Dolar hoy = 852
    else{
        // Transformamos a dolares dividiendo la cantidad de pesos chilenos
        cambio_2 = cambio_2 / 852;
        // Lo colocamos en el input especificado para la transformación
        document.getElementById("transformado_clp").value = cambio_2;
    }
}