// Creación de función
function azar(){
    // Realizamos una variable aleatoria entre 1 y 6
    let aleatorio = Math.floor((Math.random()*6) + 1);
    // Veremos que sale en cada una de ellas y lo colocaremos
    if(aleatorio == 1){
        // Si es 1, colocamos la cara de 1 cambiando la ruta de la imagen
        document.getElementById("dados").src="../fotos/cara_1.jpg";
    }
    else if(aleatorio == 2){
        // Colocamos la cara de 2
        document.getElementById("dados").src="../fotos/cara_2.png";
    }
    else if(aleatorio == 3){
        // Colocamos la cara de 3
        document.getElementById("dados").src="../fotos/cara_3.png";
    }
    else if(aleatorio == 4){
        // Colocamos la cara de 4
        document.getElementById("dados").src="../fotos/cara_4.png";
    }
    else if(aleatorio == 5){
        // Colocamos la cara de 5
        document.getElementById("dados").src="../fotos/cara_5.jpg";
    }else{
        // Colocamos la cara de 6
        document.getElementById("dados").src="../fotos/cara_6.jpeg";
    }
}


