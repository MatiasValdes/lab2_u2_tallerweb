// Creación de función que sumará dos numeros
function mayor_que_todos(){
    // Obtenemos los valores de los input
    var num_1 = document.getElementById("numero1").value;
    var num_2 = document.getElementById("numero2").value;
    var num_3 = document.getElementById("numero3").value;
    // Validar de que los dos campos vengan llenos
    if(num_1 == "" || num_2 == "" || num_3 ==""){
        alert("No ha rellenado un campo por lo que es imposible calcular")
    } else{ // Si vienen con datos, los transformamos a enteros
        num_1 = parseInt(num_1);
        num_2 = parseInt(num_2);
        num_3 = parseInt(num_3);
    
        // Validar de que ninguno de los datos ingresados no sea nulo, es decir, que no haya traido caracteres
        if(isNaN(num_1)==true || isNaN(num_2)==true || isNaN(num_3)==true){
            alert("Ha ingresado un dato no númerico")
        }else{
            // Si no ha traido caracteres, podmos empezar a buscar cual es el mayor
            // Numero 1 mayor que todos
            if(num_1 > num_2 && num_1 > num_3){
                // Aviso
                alert(num_1 + " es mayor que " + num_2 + " y que " + num_3);
            // Numero 2 sea mayor que todos
            } else if (num_2 > num_1 && num_2 > num_3){
                // Aviso
                alert(num_2 + " es mayor que " + num_1 + " y que " + num_3);
            
            } // Numero 3 mayor que todos 
            else if (num_3 > num_1 && num_3 > num_2){
                alert(num_3 + " es mayor que " + num_1 + " y que " + num_2);
            } 
            // Si es que ambos son iguales
            else{
                // Aviso
                alert("Todos los numeros ingresados son iguales");
            }
        }
    }
}