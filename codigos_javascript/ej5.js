// Creación de la función para el ejercicio 1
function divisible(){
    // Definimos la variable que guardará el numero
    let numero = document.getElementById("numero1").value; 
    // Validamos de que no venga vacio
    if(numero == ""){
        alert("No ha ingresado nada")
    }else{ // Si viene con algo, lo transformamos a numerico
        // Lo transformamos a numerico
        numero = parseInt(numero);
        // Revisamos si se pudo transformar correctamente
        if(isNaN(numero)==true){
            alert("Ha ingresado un caracter que no se ha podido transformar a numerico");
        }else{ // Si paso todas las pruebas podemos ver si es divisible
            if (numero % 2 == 0){
                // Si el modulo es 0, es porque es divisible por 2
                alert("El numero " + numero + " si es divisible por 2")
            } // Si es que no es 0, no es divisible por 2
            else{
                alert("El numero " + numero + " no es divisible por 2")
            }
        }
    }
}
