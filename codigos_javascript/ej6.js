// Creacion de funcion
function buscar_a(){
    // Pedimos la frase
    let frase = document.getElementById("frase").value;
    // Validamos de que no venga vacio el input
    if(frase ==""){
        alert("Favor de ingresar algo");
    }else{ // Si viene lleno, vemos cuantas A tiene lo que ingresó
        // Creamos un contador para ir registrando la cantidad de a
        let contador = 0
        // Recorreremos la frase y buscaremos la a
        for(let i = 0; i < frase.length;i++){
            // ocuparemos tolowercase para transformar todos los caracteres a minusculas y comparar
            // Si es una A aumentará el contador
            if(frase[i].toLowerCase() == "a"){
                // Aumento de contador
                contador++;
            }
        }
        // Luego que termine de recorrer se da aviso de la cantidad de A
        alert("La cantidad de 'A' en la frase " + frase + " es de " + contador);
    }
}
