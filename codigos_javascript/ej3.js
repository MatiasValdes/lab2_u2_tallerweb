// Creación de función que sumará dos numeros
function mayor(){
    // Obtenemos los valores de los input
    var num_1 = document.getElementById("numero1").value;
    var num_2 = document.getElementById("numero2").value;
    
    // Validar de que los dos campos vengan llenos
    if(num_1 == "" || num_2 == ""){
        alert("No ha rellenado un campo por lo que es imposible calcular")
    } // Si no son vacios, hay que validar que no sean caracteres
    else{
        // Transformamos a numerico
        num_1 = parseInt(num_1);
        num_2 = parseInt(num_2);
        // Validamos que no sean caracteres
        if(isNaN(num_1)==true || isNaN(num_2)==true){
            alert("Ha ingresado un dato no númerico")
        }else{// Si pasa todas las pruebas previas, podemos calcular cual es mayor
            // Condicional
            if(num_1 > num_2){
                // Aviso
                alert(num_1 + " es mayor que " + num_2);
            } else if (num_2 > num_1){
                // Caso contrario al que está en condicional
                alert(num_2 + " es mayor que " + num_1);
            // Si es que ambos son iguales
            } else{
                alert("Ambos numeros ingresados son iguales");
            }
        }
    }
}