// Creacion de funcion
function buscar_existencia(){
    // Creamos un contador para ir registrando la cantidad de vocales
    let contador_A = 0;
    let contador_E = 0;
    let contador_I = 0;
    let contador_O = 0;
    let contador_U = 0;
    // Pedimos la frase
    let frase = document.getElementById("frase").value;
    // Validamos de que no venga vacio el input
    if(frase ==""){
        alert("Favor de ingresar algo");
    }
    else{
        // Recorreremos la frase y buscaremos las vocales
        for(let i = 0; i < frase.length;i++){
            // ocuparemos tolowercase para transformar todos los caracteres a minusculas y comparar
            // Iremos buscando las vocales, primeramente la A
            if(frase[i].toLowerCase() == "a"){
                // Aumento de contador
                contador_A++;
            } // Si es que es una E se aumenta su contador  
            else if(frase[i].toLowerCase() == "e"){
                contador_E++;
            } // SI es que es una I se aumenta su contador
            else if(frase[i].toLowerCase() == "i"){
                contador_I++;
            }// Si es que es una O se aumenta
            else if(frase[i].toLowerCase() == "o"){
                contador_O++;
            } // Si es que es una U se aumenta
            else if(frase[i].toLowerCase() == "u"){
                contador_U++;
            }
        }
        // Iremos revisando si los contadores son mayores que 0
        // Si es que lo son se irán entregando las alertas de que existe cada una de ellas
        if(contador_A > 0){
            alert("En la frase " + frase + " si existe la vocal A");
        }
        if(contador_E > 0){
            alert("En la frase " + frase + " si existe la vocal E");
        }
        if(contador_I > 0){
            alert("En la frase " + frase + " si existe la vocal I");
        }
        if(contador_O > 0){
            alert("En la frase " + frase + " si existe la vocal O");
        }
        if(contador_U > 0){
            alert("En la frase " + frase + " si existe la vocal U");
        }
        if(contador_A==0 && contador_E ==0 && contador_I ==0 && contador_O ==0 && contador_U ==0){
            alert("No se encontraron vocales en lo que ingresó")
        }
    }
}
